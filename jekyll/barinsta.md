---
title: Information regarding Barinsta
description: This page details what happened with Barinsta and the current situation.
---

On July 26, 2021, I received a cease & desist letter[^1] from *Perkins Coie LLP*, a law firm representing Facebook. As a result, Barinsta is no longer maintained or distributed by me, and no updates will be provided to the `me.austinhuang.instagrabber` package. All crash reports sent to `barinsta@austinhuang.me` after that date are ignored[^2].

Those in possession of the source code and/or the binaries should be aware of the terms and conditions set out by the [GNU General Public License, Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html), which remains applicable to Barinsta. Nevertheless, the app is expected to function for the foreseeable future.

The developers of Barinsta sincerely appreciate your continuous support.

---

Amended May 22, 2022: I have received a lot of emails from others who are in the same situation asking me what to do. I'll write them here, noting that **this is NOT legal advice**:

1. **Take down your app to the best of _your ability_, as soon as possible.**
2. **Don't say another word to them.** This means, in particular, that you **don't** respond to any emails from them, present or future.
3. Please consult a lawyer, especially if you have significant revenue from your app. (If you, like me, are making a non-profit personal-use app, unfortunately advocacy representations are very rare, so don't get your hopes too high.)

Please understand, that being banned off Facebook is a blessing, not a curse. Meanwhile, check out [the greener pasture](https://joinfediverse.wiki/Welcome_to_the_Fediverse_Quickstart_guide)!

---

Amended January 30, 2024: Louis Barclay, the maker of *Unfollow Everything*, summarized his experience in [this article](https://12challenges.substack.com/p/how-to-deal-with-receiving-a-cease). If you're in a similar situation, I recommend you to consult it as well.

My event timeline is as follows (all times EST):

1. On July 26, 2021 at around 3pm, all Facebook and Instagram accounts belonging to me and my project partner were suspended. At 5pm I received the cease & desist[^1] on all my published email addresses[^3]. I took down the repository immediately.
2. I [requested F-Droid to remove the app](https://gitlab.com/fdroid/fdroiddata/-/issues/2440), which they refused[^4].
3. I contacted the EFF. One staff member provided me with a list of three attorneys; another tried to get me into contact with the press. For the former, one of them responded saying they cannot take my case; the other two simply did not respond. For the latter, I was told that the journalists will contact me; I only received information from one journalist who never followed up either[^5].
4. I contacted [Software Freedom Law Center](https://softwarefreedom.org/), and they referred an attorney to me.
5. I consulted with various people (none are legal professionals but one is a law student) on how to proceed in the short term, and decided on writing a general response that avoids answering any specifics. Now, as I've written above, you shouldn't respond to them, but at that time I did. The talking points were, that
* The app is non-profit,
* I don't mean anything malicious, and
* I was just a college student.
6. On July 28 at 9:44pm, I received the following response from Meta's lawyer:
> Thank you for your response. Can you please provide a detailed response to the questions posed in Facebook’s letter?
>
> Additionally, I wanted to call to your attention the website here which still appears to be active: (F-Droid link)
7. The attorney from (4) contacted me and we held a consultation. In summary, the advice was to "stay quiet." I also contacted another developer of another Facebook-related app and was given the same advice.
8. On August 24 at 1:24pm, I received the following response from Meta's lawyer:
> Following up. When can I expect a response?
9. On October 12 at 11:15pm, I received the following response from Meta's lawyer:
> Facebook has noted your claims that Barinsta was not-for-profit effort and that your Github repositories were taken down promptly. Under the circumstances, it is possible to resolve this matter in a way that restores access to your accounts, but this is not possible so long as the application remains available.
10. On January 24, 2022 at 9:20pm, I received the final response from Meta's lawyer:
> Meta Platforms, Inc. (“Meta”), formerly Facebook, Inc. and operator of the Instagram service, first wrote you on July 26, 2021 demanding that you stop providing an app, Barinsta, that improperly downloads and performs unauthorized activities on Instagram. During our correspondence, you confirmed that you have taken down the app from F-droid. However, you failed to provide any further information regarding the Barinsta as requested and the app continued to be available on GitHub[^6].
>
> We write to confirm that, continuing since the date of our original letter, your limited license to access Facebook and Instagram or use Facebook and Instagram services is and remains revoked. This means that you, your agents, your employees and/or anyone acting on your behalf may not access Facebook or Instagram’s website(s), or use any of the services offered by Facebook and Instagram, at any time for any reason.
>
> Be advised: Meta reserves the right to take whatever measures they deem necessary to protect their rights and users. This letter is not intended by Meta, and should not be construed by you, as a relinquishment or waiver of any of their rights or remedies, including but not limited to their rights to seek disgorgement of your earnings and other damages resulting from your unauthorized activities.
>
> Meta specifically reserves all such rights and remedies whether at law or in equity, under applicable domestic and foreign laws.

Some other notes:

* Some have contacted me regarding their similar situations, with most of them from developing countries. This shows that Meta practically sends these letters on a regular, near-automated basis[^7]. However, many cases I've received are commercial (ie. the developer was making money), some around SEO[^8]. I want to reiterate that there is very little advice I can give you in those situations, because making profit does give them an incentive to sue (this is why I insisted on responding to them within 48 hours of the first letter to make clear that the app is non-profit), so you *should* get a lawyer (no, advocacy lawyers are often not available to you).
* At that time I wanted to stay quiet until they stop bothering me, so I didn't really try to swing my user base in any way, though I was confident that some of them will make my case heard. However, one of them - you know who you are - made an ambitious blog post saying they will continue the effort, which garnered attention, and then abandoned it. Now, in this case they were just a kid (albeit a bit misinformed) and I don't think they're malicious, but throughout the crisis I received ridiculous comments from people, including one publicly proclaiming that I was a "coward" for not fighting back in court, and another claiming that there is a whole deep state conspiracy behind the incident... All in all, I would advise projects to be skeptical of their user base, contrary to what Louis said.

[^1]: The unredacted letter is [available online](https://github.com/austinhuang0131/austinhuang0131/issues/2). For comparison, Facebook has exhibited similar behaviour against other public-interest apps such as [Unfollow Everything](https://twitter.com/louisbarclay/status/1446110218341937170), [Swipe for Facebook](https://redd.it/mj5l64), and [Friendly](https://www.eff.org/document/eff-letter-facebook-re-friendly), as well as public-interest researches such as [NYU's Ad Observer(atory)](https://www.nytimes.com/2021/08/10/opinion/facebook-misinformation.html) and [AlgorithmWatch's Instagram monitoring project](https://algorithmwatch.org/en//instagram-research-shut-down-by-facebook).

[^2]: Those who wish to write to me privately should refer to my standard [contact methods](./#contact-me).

[^3]: Because I published all my email addresses on a public GPG key. However, to my knowledge, my project partner did not receive anything.

[^4]: At that time the issue was private, but it was published December 19, 2023. Before updating this page, I was not aware that the issue was made public, nor did I ever request it.

[^5]: Which is why there is close to no press coverage on my incident, as opposed to Louis's. In fact, all I got was just a mention in [this EFF article](https://www.eff.org/deeplinks/2021/08/facebooks-attack-research-everyones-problem) (at the bottom, it's very unnoticeable)... And yes, of course I'm jealous, but eh...

[^6]: That's exactly what they wrote.

[^7]: Even though I still get to have a good story to tell, and people would think it's pretty impressive...

[^8]: Which I have a personal disdain of, but hey, people need to make a living...
