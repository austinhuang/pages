---
layout: about
title: Hello there!
description: I can see that you have walked thousands of miles just to reach this website, but that's just my homepage. Have fun... I guess.
cover: true
permalink: /
---

<style>
@media ( max-width : 800px) {
    .resize1 {
        width: 150px !important;
    }
}
</style>

I am a masters student who [write code](#my-tech-stuff), [take photos](#my-photos), study, and [explore online stuff](#my-other-online-stuff). You may reach me using [these methods](#contact-me). Straight outta the metropolis of Shanghai, China, my current home is Montréal, QC, but I now reside in Jersey City, NJ.

See [the online version of my resume](./assets/online_resume.pdf).

## My tech stuff

<div style="float:right;padding:15px;">
<a href="https://www.zylstra.org/blog/2019/09/i-commit-to-the-tech-pledge-and-ask-you-to-do-the-same/"><img src="./assets/Pledge_badge.svg" alt="https://www.techpledge.org/" width="125" /></a>
</div>

(I believe that) I am most widely known for making [Barinsta](./barinsta.html), an open source Instagram client on Android that focuses on privacy and well-being. Ultimately I was [cease & desisted by Facebook](https://github.com/austinhuang0131/austinhuang0131/issues/2), but at least I got some clout, so that's cool.

I am now pursuing a Masters degree in Computer Science at NYU Tandon, as I continue to formalize my skillset.

I have a strong interest in finding data and organizing them to convenient formats, either to support various applications or allow others to ingest it, be it [public](https://github.com/austinhuang0131/stm-shuttle-tracker) [transport](https://github.com/steviek/PathWidgetXplat/pull/2) [data](https://github.com/steviek/PathWidgetXplat/pull/4), [archiving a mutual aid website](https://github.com/MakersMark3333/shanghai_daohouer_archive/pull/3), [converting social media posts](https://github.com/RSS-Bridge/rss-bridge/pull/2756), or [checking instances of a software](https://codeberg.org/austinhuang/matrix-serverlist/src/branch/main/pinger.js). I also combined that skill and practiced information visualization techniques in [FundVue](https://github.com/austinhuang0131/fundview), which deals with hedge fund filings.

In my free time, I make code contributions to open source software that I personally use; see my [GitHub](https://github.com/austinhuang0131) profile. My newer personal projects are on [Codeberg](https://codeberg.org/austinhuang). Beyond code, I participate in the development and adoption of various [privacy frontends](https://github.com/mendel5/alternative-front-ends), some of which I host on [bus-hit.me](https://bus-hit.me).

I used to be heavily invested in Discord and have written private Discord/Telegram bots, but since then I became a proponent of ethical platforms, namely Matrix and ActivityPub. I run [JoinMatrix.org](https://joinmatrix.org) which hosts guides to onboard new users to Matrix. I help coordinate moderation and [promotion](https://servers.joinmatrix.org/) of public Matrix homeservers. I currently manage two Mastodon instances, [Mastodon Party](https://mstdn.party) and [MSTDN+](https://mstdn.plus), as well as a user directory, [Trunk](https://communitywiki.org/trunk). I also work with other moderators on trust & safety concerns, during which I have created automated tooling to simplify content moderation; [mastodon-tag-case-corrector](https://codeberg.org/austinhuang/mastodon-tag-case-corrector) being one of them.

My laptop is a MacBook Air (M2, 13-inch, 2022) which has both macOS and Asahi Linux (Fedora Asahi Remix) available for boot. My VM stack is composed of the free stuff from Oracle (which I use to improve my Linux skills).

My phone is a Moto g 5G 2024 on stock ROM. I was a proud owner of Sony Xperia XA2 (North American H3123) until I tried to flash VoLTE support onto it just so that I could use it in the US, which not only didn't work, but in the process I lost all my photos (save the ones I posted on Mastodon before). Death to planned obsolescence!

## My photos

I have a white Pentax K-50. Long time ago I uploaded some ("Good" ones, due to Flickr's free restrictions) of my photos on [Flickr](https://flic.kr/austin0131) as well as [Unsplash](https://unsplash.com/@austinhuang). More recent ones are on my Mastodon.

## My other online stuff

* <a rel="me" href="https://mstdn.party/@austin">Mastodon</a> at `@austin@mstdn.party`: Opinions and thoughts. (Previously at `@austin@mstdn.ca`, `@a@mastodon.nz` and `@austin@ieji.de`)
    * Via Bluesky: [@ap.austinhuang.me](https://bsky.app/profile/ap.austinhuang.me)
* [OpenGeofiction](https://opengeofiction.net/user/austinhuang/history): A collaborative platform for creating fictional maps.
* [Pixiv](https://pixiv.me/montreal0131): A collection of illustrations I like.
* [Reddit](https://reddit.com/u/austinhuang)
* [YourWorldOfText](https://www.yourworldoftext.com/~austinhuang/): Ask me questions here!

## Contact me

<div class="resize1" style="float:right;padding:15px;">
<a href="https://www.fsf.org/fb"><img src="./assets/not-fd.svg" alt="You won't find me on Facebook" width="200"/></a>
</div>

Please only contact me via the accounts written on this page.

* [Email](mailto:im@austinhuang.me): `im@austinhuang.me`
* Instant messaging platforms, in roughly descending order of preference:
    * [Matrix](https://matrix.to/#/@austin:tchncs.de): `@austin:tchncs.de`
    * [Signal](https://signal.me/#eu/JnG3MhNhkGQSUPP8aaasS6d6wr8Tp7uwJ8/w4cprGl6wKrbMkKVJ/JDbwDNopvHp): `austin.8080`
    * [XMPP](xmpp:a@bendy.bus-hit.me?omemo-sid-1353477104=cf60b53c4c6e42a5e4ce6f01f8f82f5fbde59b7a7fbd9c0a8a04bcc2d6ad3a63&omemo-sid-1209606132=984f490f799212d6ea16c87cf212874209f8830a7ab95e4400493600941d8f75): `a@bendy.bus-hit.me`
    * [Telegram](https://t.me/austinhuang): `@austinhuang`
    * [Discord](https://discord.com/users/207484517898780672): `austinhuang.me`

If you want to contact me securely:

* By email: My PGP public key, ending with `84C2 3AA0 4587 A91F`, is available through the [OpenPGP Keyserver](https://keys.openpgp.org/pks/lookup?op=get&options=mr&search=0xf4c5be258540e91ab01b448584c23aa04587a91f), which also contains [notations indicating my active accounts on other platforms](https://keyoxide.org/hkp/f4c5be258540e91ab01b448584c23aa04587a91f). If I receive an encrypted email from you, I will look for your public key on that keyserver.
* By Matrix: My sessions change quite often (due to client setup), but they should always be cross-signed. If needed, please ask for my sessions through another way or in person.
* By XMPP: See URI above.

(I am no longer on Threema; I lost my license key and cannot retrieve it anymore. I no longer use Sessions.)

You can also [donate to me](/donate.html)!

## About This Site

No Cloudflare, No Google, No cookies, *No problemo.*

* The pages are [on Codeberg](https://codeberg.org/austinhuang/pages), built using their Woodpecker CI.
* This site uses nameservers from [deSec.io](https://desec.io).
* This site is available on [IPFS](https://ipfs.io/) (with [DNSLink](https://docs.ipfs.io/concepts/dnslink/) pointed at `austinhuang.me`). Try it with your local node!

### Webrings

Note that I do not control which websites the webrings link to.

This site is part of...

* [<](https://webring.bucketfish.me/redirect.html?to=prev&name=AustinHuang.me) [bucket webring](https://webring.bucketfish.me/) [>](https://webring.bucketfish.me/redirect.html?to=next&name=AustinHuang.me)
* [<](https://hotlinewebring.club/a/previous) [Hotline Webring](https://hotlinewebring.club/) [>](https://hotlinewebring.club/a/next)
* [<](https://webring.dinhe.net/prev/https://austinhuang.me) [The retronaut webring](https://webring.dinhe.net/) ([random](https://webring.dinhe.net/random)) [>](https://webring.dinhe.net/next/https://austinhuang.me)

<img src="./assets/geek_1.jpg" alt="geekring.net nvigation" usemap="#geekringmap">
<map name="geekringmap" class="no-mark-external">
    <area shape="rect" coords="9,28,111,53" alt="Previous geekring site" href="https://geekring.net/site/181/previous">
    <area shape="rect" coords="248,28,350,53" alt="Random geekring site" href="https://geekring.net/site/181/random">
    <area shape="rect" coords="490,28,592,53" alt="Next geekring site" href="https://geekring.net/site/181/next">
    <area shape="rect" coords="465,6,566,22" alt="Main geekring site" href="https://geekring.net/">
</map>

<a href="https://icp.gov.moe/?keyword=20230898" target="_blank">萌ICP备20230898号</a>