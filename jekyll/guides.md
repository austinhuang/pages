---
title: The Guides I offer!
description: Some freebies to take away from this website!
---

"Freebies" means you're free to view/share them. Please respect [the license](./license.html).

## Join Matrix!

Visit [JoinMatrix.org](https://joinmatrix.org).

## The Discord Trilogy

The advertising and community guides have been removed as I have not updated them for 3 years and now I only use Discord out of necessity.

* [Issues](./discord-issues.html): Internal corruption and faults of Discord.

## Others

* [BVE Trainsim 5 Survival Guide](./bve.html): Ditch the expensive Steam simulators and embrace this Japanese masterpiece!
